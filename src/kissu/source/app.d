import std.stdio : writefln, writeln, stderr;
import std.format;
import std.getopt;
import std.conv: ConvException;
import d2sqlite3;
import std.typecons : Nullable, nullable;
import core.exception: AssertError;

import service: Service;
import client: Client;
import client_getopt;
import service_getopt;
import std.json;
import prettyJSON: writePrettyJSON, toPrettyJSON;
import std.algorithm.searching: canFind;

int main(string[] args)
{
  if (args.length < 3) {
    stderr.writefln("Not Enough Arguments! Run `%s client|service --help` for more information.", args[0]);
    return 1;
  }
  switch(args[1]) {
  case "client": {
    auto operation = args[2];
    if( ["get", "create", "update", "delete"].canFind(operation)) {
      auto nullableClient = client_handle_opts(operation, args);
      if (!nullableClient.isNull) {
	auto client = nullableClient.get;
	if (operation == "create") {
	  auto saved = client.save();
	  if (saved == Client.SAVE_STATES.NOT_CREATED) return 2;
	  return 0;
	}
	if (operation == "update") {
	  auto saved = client.save(false);
	  if (saved == Client.SAVE_STATES.NOT_UPDATED) return 3;
	  return 0;
	}
	else if (operation == "get") {
	  client.writePrettyJSON();
	  return 0;
	}
	else if (operation == "delete") {
	  client.remove();
	  return 0;
	}
      }
      else {
	return 4;
      }
    }
    else {
      stderr.writeln("Error! Client Operation Not Understood!");
      return 5;
    }
    break;
  }
  case "service":
    {
      auto operation = args[2];
      if( ["get", "create", "update", "delete"].canFind(operation)) {
	auto nullableServiceOption = service_handle_opts(operation, args);
	if (nullableServiceOption.isNull) return 6;
	auto nullableService = nullableServiceOption.get.service;
	if (!nullableService.isNull) {
	  auto service = nullableService.get;
	  if (operation == "get") {
	    if (nullableServiceOption.get.generate_m4){
	      import m4Macro;
	      service.getM4Macro!(Service).writeln;
	    } else service.writePrettyJSON;
	    return 0;
	  }
	  if (operation == "delete") service.remove();
	  if (operation == "create") {
	    auto saved = service.save();
	    if (saved == Service.SAVE_STATES.NOT_CREATED) return 7;
	    return 0;
	  }
	  if (operation == "update") {
	    auto saved = service.save(false);
	    if (saved == Service.SAVE_STATES.NOT_UPDATED) return 8;
	    return 0;
	  }
	} else return 9;
      }
      else if (operation == "list") {
	import std.algorithm.iteration: map, joiner;
	(_ => writefln("[%s]", _))(Service.all()[].map!(s => s.toPrettyJSON).joiner(","));
	return 0;
      }
      break;
    }
  default: {
    stderr.writefln("Argument Mismatch! Run `%s client|service --help` for more information.", args[0]);
    return 23;
  }
  }
  return 0;
}
